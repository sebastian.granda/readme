# Readme

Corta descripción del proyecto

## Instalación
Para este repositorio debemos usar el manejador de paquetes [pip](https://pip.pypa.io/en/stable/) para instalar las dependecias.

```bash
pip install -r requirements.txt
```

## Como usar

```bash
flask run
```

### Endpoint de utilidad
```
/auth/login # descripción del endpoint
```

## Variables de entorno
|Nombre|Tipo|Descripción|
|------|----|-----------|
|FLASK_MAIN|STR|Archivo para iniciar flask|
|FLASK_DEBUG|INT|Identifica si flask esta en desarrollo o producción|


## Contribución
@cristian.chavez1

## Flujo de trabajo

- `main` es la rama donde la versió estable
